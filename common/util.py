def isNumber(num):
    try:
        float(num)
        return True  # num을 float으로 변환할 수 있는 경우
    except ValueError:  # num을 float으로 변환할 수 없는 경우
        return False


def get_gap_price(input_price):
    price = float(input_price)
    if price < 10:
        price = round(price, 2)
    elif price < 100:
        price = round(price, 1)
    elif price < 1000:
        price = round(price)
    elif price < 10000:
        price = int(price / 5) * 5
    elif price < 100000:
        price = int(price / 10) * 10
    elif price < 500000:
        price = int(price / 50) * 50
    elif price < 1000000:
        price = int(price / 100) * 100
    elif price < 2000000:
        price = int(price / 500) * 500
    else:
        price = int(price / 1000) * 1000
    return price


def getGap(input_price):
    price = float(input_price)
    if price < 10:
        price = 0.01
    elif price < 100:
        price = 0.1
    elif price < 1000:
        price = 1
    elif price < 10000:
        price = 5
    elif price < 100000:
        price = 10
    elif price < 500000:
        price = 50
    elif price < 1000000:
        price = 100
    elif price < 2000000:
        price = 500
    else:
        price = 1000
    return price


def getGapUnit(price, unit):
    gap = getGap(price)
    unit = gap * float(unit)
    if gap > 1:
        unit = round(unit)
    return unit
