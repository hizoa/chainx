from PyQt5.QtWidgets import *
from qtpy import QtGui
from common.util import *
from PyQt5.QtCore import *


def loadOrderTable(table, jsonData):
    rSize = 20
    cSize = 3
    bidList = []
    askList = []
    for data in jsonData['buys']:
        dic = {"price": get_gap_price(data['price']), "volume": round(float(data['volume']), 4), "color": "blue"}
        bidList.append(dic)
    for data in jsonData['sells']:
        dic = {"price": get_gap_price(data['price']), "volume": round(float(data['volume']), 4), "color": "red"}
        askList.append(dic)
    table.setRowCount(rSize)
    table.setColumnCount(cSize)
    table.rowHeight(10)
    table.setHorizontalHeaderLabels(["매도", "호가", "매수"])

    for i, data in enumerate(askList):
        price = QTableWidgetItem(str(data['price']))
        price.setTextAlignment(Qt.AlignCenter)
        volume = QTableWidgetItem(str(data['volume']))
        price.setBackground(QtGui.QColor(243, 251, 255))
        volume.setTextAlignment(Qt.AlignVCenter | Qt.AlignRight)
        table.setItem(9 - i, 1, price)
        table.setItem(9 - i, 0, volume)
    for i, data in enumerate(bidList):
        price = QTableWidgetItem(str(data['price']))
        price.setTextAlignment(Qt.AlignCenter)
        price.setBackground(QtGui.QColor(255, 244, 248))
        volume = QTableWidgetItem(str(data['volume']))
        volume.setTextAlignment(Qt.AlignVCenter | Qt.AlignLeft)
        table.setItem(10 + i, 1, price)
        table.setItem(10 + i, 2, volume)

    table.resizeRowsToContents()
