from PyQt5.QtCore import QAbstractListModel
from PyQt5.QtCore import QVariant
from PyQt5.QtGui import QBrush
from PyQt5.QtGui import QColor
from PyQt5.QtCore import Qt


class LogModel(QAbstractListModel):
    def __init__(self, data=None, parent=None):
        QAbstractListModel.__init__(self, parent)
        self._data = data

    def rowCount(self, parent=None, *args, **kwargs):
        return len(self._data)

    def data(self, QModelIndex, role=None):
        item = self._data[QModelIndex.row()]

        if role == Qt.DisplayRole:
            return "%s" % (item['name'])
        elif role == Qt.DecorationRole:
            return QColor(item['color'])
        return QVariant()
