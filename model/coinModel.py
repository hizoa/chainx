from PyQt5.QtCore import Qt
from PyQt5.QtGui import QStandardItemModel
from PyQt5.QtGui import QStandardItem
from PyQt5.QtCore import QVariant


class CoinModel(QStandardItemModel):
    def __init__(self, data=None, parent=None):
        QStandardItemModel.__init__(self, parent)
        for i, d in enumerate(data):
            col_1 = QStandardItem(d["symbol"])
            col_2 = QStandardItem(d["name"])
            col_3 = QStandardItem(d["balance"])
            self.setItem(i, 0, col_1)
            self.setItem(i, 1, col_2)
            self.setItem(i, 2, col_3)
            self.setHorizontalHeaderLabels(["Symbol", "Name", "Balance"])

    def data(self, QModelIndex, role=None):
        data = self.itemData(QModelIndex)
        if role == Qt.DisplayRole:
            return data[0]
        if role == Qt.UserRole:
            return data
        return QVariant()
