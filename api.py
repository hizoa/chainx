import random
import requests
import http.client
import json
from common.util import *
import http.client
import json
import random
import math

import requests

from common.util import *

API_PATH = "production-api.chainx.kr"
HTTP_API_PATH = "http://" + API_PATH

class API:

    def __init__(self, accessToken, userUuid):
        self.token = accessToken
        self.uuid = userUuid
        self.conn = http.client.HTTPSConnection(API_PATH)
        self.headers = {
            'authorization': "Bearer " + accessToken + "",
            'content-type': "application/json"
        }
        self.exchange = 0

    def buy(self, coin, price, volume):
        payload = "{\"trading_pair\":\"" + coin + "-KRW\",\"side\":\"BUY\",\"volume\":" + str(
            volume) + ",\"price\":" + str(price) + ",\"order_type\":\"LIMIT\"}"
        self.conn.request("POST", "/api/v1.0/orders/", payload, self.headers)
        res = self.conn.getresponse()
        data = res.read()
        check = data.decode("utf-8")
        print(check)
        return check

    def sell(self, coin, price, volume):
        payload = "{\"trading_pair\":\"" + coin + "-KRW\",\"side\":\"SELL\",\"volume\":" + str(
            volume) + ",\"price\":" + str(price) + ",\"order_type\":\"LIMIT\"}"
        self.conn.request("POST", "/api/v1.0/orders/", payload, self.headers)
        res = self.conn.getresponse()
        data = res.read()
        check = data.decode("utf-8")
        print(check)
        return check

    def cancel(self, coin, side, price, volume):
        # payload = "{\"trading_pair\":\"" + coin + "-KRW\",\"side\":\"SELL\",\"volume\":" + str(
        #     volume) + ",\"price\":" + str(price) + ",\"order_type\":\"LIMIT\"}"
        payload = "{\"trading_pair\":\"" + coin + "-KRW\",\"side\":\"SELL\",\"volume\":\"1\",\"price\":\"2905\",\"order_type\":\"LIMIT\"}"
        self.conn.request("DELETE", "/api/v1.0/orders/%7B" + self.uuid + "%7D/", payload, self.headers)
        res = self.conn.getresponse()
        data = res.read()
        check = data.decode("utf-8")
        print(check)
        return check

    @staticmethod
    def best(coin):
        res = requests.get(HTTP_API_PATH + "/api/v1.0/trading_pairs/best/?trading_pair_name=" + coin + "-KRW")
        data = res.json()
        if 'best_bid_price' in data and 'best_ask_price' in data:
            return data
        return None

    @staticmethod
    def last(coin):
        res = requests.get(HTTP_API_PATH + "/api/v1.0/trades/last/?trading_pair_name=" + coin + "-KRW")
        if res.status_code == 200:
            data = res.json()
            return data
        return None

    @staticmethod
    def lastPrice(coin):
        res = requests.get(HTTP_API_PATH + "/api/v1.0/trades/last/?trading_pair_name=" + coin + "-KRW")
        if res.status_code == 200:
            data = res.json()
            return get_gap_price(data['price'])
        return None

    def account(self):
        payload = "{}"
        self.conn.request("GET", "/api/v1.0/users/%7B" + self.uuid + "%7D/accounts/", payload, self.headers)
        res = self.conn.getresponse()
        if res.getcode() == 200:
            jsonData = json.loads(res.read())
            dic = {}
            for data in jsonData:
                dic[data['asset_symbol']] = round(float(data['liquid']), 4)
            return dic
        data = res.read()
        check = data.decode("utf-8")
        print(check)
        return None

    def accountKrw(self):
        payload = "{}"
        self.conn.request("GET", "/api/v1.0/users/%7B" + self.uuid + "%7D/accounts/", payload, self.headers)
        res = self.conn.getresponse()
        if res.getcode() == 200:
            jsonData = json.loads(res.read())
            dic = {}
            for data in jsonData:
                dic[data['asset_symbol']] = round(float(data['liquid']), 4)
            return dic['KRW']
        data = res.read()
        check = data.decode("utf-8")
        print(check)
        return 0.0

    def accountSpecific(self):
        payload = "{}"
        self.conn.request("GET", "/api/v1.0/users/%7" + self.uuid + "%7D/accounts/", payload, self.headers)
        res = self.conn.getresponse()
        if res.getcode() == 200:
            jsonData = json.loads(res.read())
            accountList = []
            for data in jsonData:
                dic = {"symbol": data['asset_symbol'], "name": data['asset_korean_name'],
                       "balance": str(round(float(data['liquid']), 4))}
                accountList.append(dic)
            return accountList
        data = res.read()
        check = data.decode("utf-8")
        print(check)
        return check

    def getOrderBook(self, index):
        res = requests.get(HTTP_API_PATH + "/api/v1.0/trading_pairs/orderbook/?trading_pair_name=" + index + "-KRW")
        jsonData = res.json()
        if 'buys' in jsonData and 'sells' in jsonData:
            return jsonData
        return None

    def getPrice(self, symbol):

        if symbol == "BTC" or symbol == "ETH":
            res = requests.get("https://crix-api-endpoint.upbit.com/v1/crix/candles/minutes/1?code=CRIX.UPBIT.KRW-" + symbol)
            if res.status_code == 200:
                data = res.json()
                price = round(data[0]['tradePrice'])
                dic = {"price": str(price), "message": "업비트:" + str(price)}
                print(data)
                return dic
        elif symbol == "TRY":
            resp = requests.get("https://api.kucoin.com/api/v1/market/orderbook/level1?symbol=TRY-USDT")
            if resp.status_code == 200:
                coin = resp.json()
                price = coin['data']['price']
                if price is not None:
                    kor_price = str(get_gap_price(float(price) * float(self.exchange)))
                    dic = {"price": str(kor_price), "message": "쿠코인:" + str(kor_price)}
                    return dic
        else:
            res = requests.get(
                "https://api.coinhills.com/v1/cspa/" + symbol.lower())
            if res.status_code == 200:
                data = res.json()
                if data['success']:
                    cspaSymbol = "CSPA:" + symbol
                    price = data['data'][cspaSymbol]['cspa']
                    kor_price = str(get_gap_price(float(price) * float(self.exchange)))
                    dic = {"price": str(kor_price), "message": "코인힐스:" + str(kor_price)}
                    return dic
                else:
                    dic = {"price": '0', "message": "코인정보 없음:" + '0'}
                    return dic


            # headers = {
            #     'Accepts': 'application/json',
            #     'X-CMC_PRO_API_KEY': self.api
            # }
            # url = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest?symbol=BTC,TRY,ETH,FTM,SKM,MXC&convert=KRW'
            # session = Session()
            # session.headers.update(headers)
            # try:
            #     res = session.get(url)
            #     if res.status_code == 200:
            #         response = res.json()
            #         if symbol in response['data']:
            #             data = response['data'][symbol]['quote']['KRW']['price']
            #             price = get_gap_price(data)
            #             dic = {"price": str(price), "message": "코인마켓캡:" + str(price)}
            #             print(data)
            #             return dic
            #     if res.status_code == 429:
            #         dic = {"price": "", "message": "코인마켓 API 만료"}
            #         return dic
            # except (ConnectionError, Timeout, TooManyRedirects) as e:
            #     print(e)
        return None

    def buyOrder(self, coinIndex, start, end, volumeS, volumeE, val, check):
        lastPrice = float(start)
        while lastPrice <= float(end):
            randVolume = random.uniform(float(volumeS), float(volumeE))
            if check is True:
                randVolume = int(randVolume)
            gap = getGap(lastPrice)
            res = self.buy(coinIndex, lastPrice, randVolume)
            if res.__contains__('error_code'):
                return res
            lastPrice = round(lastPrice + (gap * float(val)), 4)
        return "ok"

    def sellOrder(self, coinIndex, start, end, volumeS, volumeE, val, check):
        lastPrice = float(start)
        while lastPrice <= float(end):
            randVolume = random.uniform(float(volumeS), float(volumeE))
            if check is True:
                randVolume = int(randVolume)
            gap = getGap(lastPrice)
            res = self.sell(coinIndex, lastPrice, randVolume)
            if res.__contains__('error_code'):
                return res
            lastPrice = lastPrice + (gap * float(val))
            lastPrice = round(lastPrice, 2)
        return "ok"

    def getFrx(self):
        res = requests.get("https://quotation-api-cdn.dunamu.com/v1/forex/recent?codes=FRX.KRWUSD")
        if res.status_code == 200:
            jsonData = res.json()
            self.exchange = jsonData[0]['basePrice']