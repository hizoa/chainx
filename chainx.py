import sys
from PyQt5.uic import loadUi
from PyQt5.QtGui import QIcon
from api import API
from log.log import *
from model.coinModel import CoinModel
from order.orderTable import *
from model.logModel import LogModel
from threadClass.worker import Worker
from threadClass.updown import UpDown
from common.util import *


class MyWindow(QMainWindow):

    def __init__(self):
        super().__init__()
        self.worker = Worker()
        self.worker.finished.connect(self.update_message)
        self.worker.checked.connect(self.check_status)
        self.upDown = UpDown()
        self.upDown.finished.connect(self.update_message)
        self.setWindowIcon(QIcon('header_logo.png.'))
        self.chainApi = API("", "")
        loadUi("current.ui", self)
        self.startButton.clicked.connect(self.auto_start)
        self.stopButton.clicked.connect(self.auto_stop)
        self.loadButton.clicked.connect(self.btnLoadClicked)
        self.btnBuyOrder.clicked.connect(self.btnBuyOrderClicked)
        self.btnSellOrder.clicked.connect(self.btnSellOrderClicked)
        self.pushButtonAccount.clicked.connect(self.btnAccountClicked)
        self.btnUpDownBuy.clicked.connect(self.btnUpDownBuyClicked)
        self.btnUpDownSell.clicked.connect(self.btnUpDownSellClicked)
        self.btnUpDownStop.clicked.connect(self.btnUpDownStopClicked)
        self.leSelfVoume1.textChanged.connect(self.selfTradeInputChanged)
        self.leSelfVoume2.textChanged.connect(self.selfTradeInputChanged)
        self.leMinPrice.textChanged.connect(self.selfTradeInputChanged)
        self.leMaxPrice.textChanged.connect(self.selfTradeInputChanged)
        self.leBuyVali.textChanged.connect(self.orderFlowInputChanged)
        self.leSellVali.textChanged.connect(self.orderFlowInputChanged)
        self.leBuyS.textChanged.connect(self.orderFlowInputChanged)
        self.leSellS.textChanged.connect(self.orderFlowInputChanged)
        self.comboBoxOrder.currentTextChanged.connect(self.comboBoxChanged)

        self.textEditMinTime.setText('1')
        self.textEditMaxTime.setText('3')
        cur_time = QTime.currentTime()
        str_time = cur_time.toString("hh:mm:ss")
        self.statusBar().showMessage(str_time)
        self.checkBox.setEnabled(False)
        self.total = 0
        self.coinIndex = ""
        self.logList = []
        self.coinList = []
        self.loadSaveData()

    def auto_start(self):
        if self.checkInput():
            self.worker.init_value(self.textEditAccess.text(), self.textEditUuid.text(),
                                   self.coinIndex, self.leMinPrice.text(), self.leMaxPrice.text(),
                                   self.leSelfVoume1.text(), self.leSelfVoume2.text(),
                                   self.textEditMinTime.text(), self.textEditMaxTime.text(),
                                   self.checkBoxBid.isChecked(), self.checkBoxAsk.isChecked(),
                                   self.cbPointSelf.isChecked())
            saveCoinInfo(self.coinIndex, self.leMinPrice.text(), self.leMaxPrice.text(),
                                   self.leSelfVoume1.text(), self.leSelfVoume2.text(), self.editTextGoal.text())
            self.worker.start_thread()
            self.total = 0
            self.labelTotal.setText(str(self.total))
            self.logList.clear()
            self.pbGoal.setValue(0)
            self.logList.append({"name": "시작원화 " + self.label_krw.text(), "color": "black", "bg_color": "yellow"})
            model = LogModel(self.logList)
            self.lvLog.setModel(model)

    def auto_stop(self):
        self.worker.stop()
        self.msg_box("중지되었습니다.")
        self.btnLoadClicked()
        sendLog(self.textEditAccess.text(), self.textEditUuid.text())

    def loadSaveData(self):
        dic = loadDb()
        if dic is not None:
            self.textEditAccess.setText(dic['token'])
            self.textEditUuid.setText(dic['uuid'])
        if self.textEditAccess.text() != "" or self.textEditUuid.text() != "":
            self.btnAccountClicked()

    @pyqtSlot(str)
    def update_message(self, check):
        if check.__contains__('error_code'):
            self.worker.stop()
            self.upDown.stop()
            self.msg_box(check)
        elif check.__contains__('감지'):
            if check.__contains__('매도'):
                self.logList.append({"name": check, "color": "blue", "bg_color": "yellow"})
                model = LogModel(self.logList)
                self.lvLog.setModel(model)
            elif check.__contains__('매수'):
                self.logList.append({"name": check, "color": "red", "bg_color": "yellow"})
                model = LogModel(self.logList)
                self.lvLog.setModel(model)
        elif check.__contains__('완료'):
            self.msg_box(check)
            self.refreshOrderBook(self.coinIndex)


    @pyqtSlot(float, float)
    def check_status(self, volume, price):
        self.total = self.total + round(volume * price)
        self.labelTotal.setText(format(self.total, ',') + "원")

        if self.checkBox.isChecked():
            self.load_price()
        self.worker.init_value(self.textEditAccess.text(), self.textEditUuid.text(),
                               self.coinIndex, self.leMinPrice.text(), self.leMaxPrice.text(),
                               self.leSelfVoume1.text(), self.leSelfVoume2.text(),
                               self.textEditMinTime.text(), self.textEditMaxTime.text(),
                               self.checkBoxBid.isChecked(), self.checkBoxAsk.isChecked(),
                               self.cbPointSelf.isChecked())

        if self.checkBoxGoal.isChecked() and isNumber(self.editTextGoal.text()):
            goal_money = float(self.editTextGoal.text())
            self.pbGoal.setValue(int(self.total/int(goal_money) * 100))
            if goal_money < self.total:
                self.pbGoal.setValue(100)
                self.auto_stop()

    @pyqtSlot()
    def refreshAutoOrder(self):
        self.refreshOrderBook(self.coinIndex)

    def comboBoxChanged(self, index):
        if type(index) is not str:
            return
        self.worker.stop()
        self.coinIndex = index
        self.refreshOrderBook(index)
        for data in self.coinList:
            if data['symbol'] == index:
                self.lbCoin.setText(data['name'])
                break
        self.labelTotal.setText("")
        dic = loadCoinInfo(index)
        if type(dic) is dict:
            self.leMinPrice.setText(dic['minPrice'])
            self.leMaxPrice.setText(dic['maxPrice'])
            self.leSelfVoume1.setText(dic['minVol'])
            self.leSelfVoume2.setText(dic['maxVol'])
            self.editTextGoal.setText(dic['goal'])
        self.btnLoadClicked()
        self.selfTradeInputChanged()

    def refreshOrderBook(self, index):
        self.twOrder.clear()
        self.leSellS.clear()
        self.leSellE.clear()
        self.leSellVolumeS.clear()
        self.leSellVolumeE.clear()
        self.leSellVali.clear()
        self.leBuyS.clear()
        self.leBuyE.clear()
        self.leBuyVolumeS.clear()
        self.leBuyVolumeE.clear()
        self.leBuyVali.clear()
        jsonData = self.chainApi.getOrderBook(index)
        if jsonData is None:
            return
        loadOrderTable(self.twOrder, jsonData)

    def btnAccountClicked(self):
        self.chainApi = API(self.textEditAccess.text(), self.textEditUuid.text())
        data = self.chainApi.accountSpecific()
        if type(data) is list:
            self.coinList = data
            model = CoinModel(data)
            view = QTableView()
            view.setSelectionBehavior(view.SelectRows)  # 한 줄 단위로 선택
            self.comboBoxOrder.setView(view)
            self.comboBoxOrder.setModel(model)
            createCoinTable(data)
        elif type(data) is str:
            self.comboBoxOrder.clear()
            self.msg_box(data)

    def btnLoadClicked(self):
        setDb(self.textEditAccess.text(), self.textEditUuid.text())
        self.chainApi = API(self.textEditAccess.text(), self.textEditUuid.text())
        self.load_price()
        self.refreshOrderBook(self.coinIndex)
        dic = self.chainApi.account()
        if dic is not None:
            if self.coinIndex in dic:
                self.label_have.setText(str(dic[self.coinIndex]))
                self.label_krw.setText(str((dic['KRW'])))
            else:
                self.label_have.setText('[비상장 코인]')
        else:
            self.label_have.setText('[토큰 갱신 필요!!]')

    def btnBuyOrderClicked(self):
        if self.checkOrderInput("buy"):
            res = self.chainApi.buyOrder(self.coinIndex, self.leBuyS.text(), self.leBuyE.text(), self.leBuyVolumeS.text(), self.leBuyVolumeE.text(), self.leBuyVali.text(), self.cbPoint.isChecked())
            if res.__contains__('error_code'):
                self.update_message(res)
            self.refreshOrderBook(self.coinIndex)

    def btnSellOrderClicked(self):
        if self.checkOrderInput("sell"):
            res = self.chainApi.sellOrder(self.coinIndex, self.leSellS.text(), self.leSellE.text(), self.leSellVolumeS.text(), self.leSellVolumeE.text(), self.leSellVali.text(), self.cbPoint.isChecked())
            if res.__contains__('error_code'):
                self.update_message(res)
            self.refreshOrderBook(self.coinIndex)

    def btnUpDownBuyClicked(self):
        self.upDown.init_value(self.textEditAccess.text(), self.textEditUuid.text(),
                               self.coinIndex, "BUY", self.leUpDownGoal.text(),
                               self.leUpDownVolumeS.text(), self.leUpDonwVolumeE.text(),
                               self.leUpDownTimeS.text(), self.leUpDownTimeE.text())
        self.upDown.start_thread()

    def btnUpDownSellClicked(self):
        self.upDown.init_value(self.textEditAccess.text(), self.textEditUuid.text(),
                               self.coinIndex, "SELL", self.leUpDownGoal.text(),
                               self.leUpDownVolumeS.text(), self.leUpDonwVolumeE.text(),
                               self.leUpDownTimeS.text(), self.leUpDownTimeE.text())
        self.upDown.start_thread()

    def btnUpDownStopClicked(self):
        self.upDown.stop()
        self.msg_box("중지되었습니다.")

    def load_price(self):
        name = self.coinIndex
        self.chainApi.getFrx()
        dic = self.chainApi.getPrice(name)
        if dic is not None:
            self.checkBox.setEnabled(True)
            if name == "BTC" or name == "ETH" or name == "TRY" or name == "SKM" or name == "FTM" or name == "MXC":
                self.checkBox.setChecked(True)
                self.leMinPrice.setText(dic['price'])
                self.leMaxPrice.setText(dic['price'])
            else:
                self.checkBox.setChecked(False)
            self.lbMarket.setText(dic['message'])
            self.lbMarket.setStyleSheet("color: black")
        else:
            self.lbMarket.setStyleSheet("color: red")
            self.lbMarket.setText("직접 입력하세요.")
            self.checkBox.setEnabled(False)

    def coin_changed(self):
        self.leMinPrice.setText("")
        self.leMaxPrice.setText("")
        self.leSelfVoume1.setText("")
        self.leSelfVoume2.setText("")
        self.checkBox.setEnabled(False)
        self.checkBox.setChecked(False)
        self.lbMinPrice.setText("")
        self.lbMaxPrice.setText("")
        self.labelVol1.setText("")
        self.labelVol2.setText("")

    def selfTradeInputChanged(self):
        volume = self.leSelfVoume1.text()
        price = self.leMinPrice.text()
        if isNumber(volume) and isNumber(price):
            self.labelVol1.setText(format(round(float(volume) * float(price)), ',') + "원")
            self.lbMinPrice.setText(format(round(float(price)), ",") + "원")
        else:
            self.labelVol1.setText("")
            self.lbMinPrice.setText("")
        volume = self.leSelfVoume2.text()
        price = self.leMaxPrice.text()
        if isNumber(volume) and isNumber(price):
            self.labelVol2.setText(format(round(float(volume) * float(price)), ',') + "원")
            self.lbMaxPrice.setText(format(round(float(price)), ",") + "원")
        else:
            self.labelVol2.setText("")
            self.lbMaxPrice.setText("")

    def orderFlowInputChanged(self):
        if isNumber(self.leBuyS.text()) and isNumber(self.leBuyVali.text()):
            unit = getGapUnit(self.leBuyS.text(), self.leBuyVali.text())
            self.lbBuyVali.setText(format(unit, ',') + "원")
        else:
            self.lbBuyVali.setText("")
        if isNumber(self.leSellS.text()) and isNumber(self.leSellVali.text()):
            unit = getGapUnit(self.leSellS.text(), self.leSellVali.text())
            self.lbSellVali.setText(format(unit, ',') + "원")
        else:
            self.lbSellVali.setText("")

    def checkOrderInput(self, order):
        self.chainApi = API(self.textEditAccess.text(), self.textEditUuid.text())
        krw = 0
        if isNumber(self.label_krw.text()):
            krw = float(self.label_krw.text())
        if order == 'buy':
            if not isNumber(self.leBuyS.text()) or not isNumber(self.leBuyE.text())\
                    or not isNumber(self.leBuyVolumeS.text()) or not isNumber(self.leBuyVolumeE.text())\
                    or not isNumber(self.leBuyVali.text()):
                self.msg_box("숫자를 정확하게 입력해 주세요")
                return False
            leBuyS = float(self.leBuyS.text())
            leBuyE = float(self.leBuyE.text())
            leBuyVolumeS = float(self.leBuyVolumeS.text())
            leBuyVolumeE = float(self.leBuyVolumeE.text())
            lbBuyVali = getGapUnit(self.leBuyS.text(), self.leBuyVali.text())
            if leBuyS > leBuyE or leBuyVolumeS > leBuyVolumeE:
                self.msg_box("시작-끝 범위를 확인해 주세요")
                return False
            data = self.chainApi.best(self.coinIndex)
            if data is not None:
                bestAsk = data['best_ask_price']
                if bestAsk < leBuyE:
                    self.msg_box("현재 매도 호가 범위를 침범합니다.")
                    return False

            avrAmount = leBuyVolumeE * ((leBuyS + leBuyE)/2)
            count = (leBuyE - leBuyS)/lbBuyVali
            if (avrAmount * count) > krw:
                self.msg_box("금액이 부족할 수 있습니다.")
                return False
            if self.cbPoint.isChecked():
                if not self.leBuyVolumeS.text().isdigit() or not self.leBuyVolumeE.text().isdigit():
                    self.msg_box("주문하는 수량이 소수점이면 소수점을 제거할 수 없습니다.")
                    return False

        else:
            have = 0
            if isNumber(self.label_have.text()):
                have = float(self.label_have.text())
            if not isNumber(self.leSellS.text()) or not isNumber(self.leSellE.text()) \
                    or not isNumber(self.leSellVolumeS.text()) or not isNumber(self.leSellVolumeE.text()) \
                    or not isNumber(self.leSellVali.text()):
                self.msg_box("숫자를 정확하게 입력해 주세요")
                return False
            leSellS = float(self.leSellS.text())
            leSellE = float(self.leSellE.text())
            leSellVolumeS = float(self.leSellVolumeS.text())
            leSellVolumeE = float(self.leSellVolumeE.text())
            lbSellVali = getGapUnit(self.leSellS.text(), self.leSellVali.text())
            if leSellS > leSellE or leSellVolumeS > leSellVolumeE:
                self.msg_box("시작-끝 범위를 확인해 주세요")
                return False
            data = self.chainApi.best(self.coinIndex)
            if data is not None:
                bestBid = data['best_bid_price']
                if bestBid > leSellS:
                    self.msg_box("현재 매수 호가 범위를 침범합니다.")
                    return False
            count = (leSellE - leSellS) / lbSellVali
            maxAmount = leSellVolumeE * count
            if maxAmount > have:
                self.msg_box("수량이 부족합니다. 최대 예상 수량:" + str(maxAmount))
                return False
            if self.cbPoint.isChecked():
                if not self.leSellVolumeS.text().isdigit() or not self.leSellVolumeE.text().isdigit():
                    self.msg_box("주문하는 수량이 소수점이면 소수점을 제거할 수 없습니다.")
                    return False
        return True

    def checkInput(self):
        if self.textEditAccess.text() is "":
            self.msg_box("Token을 입력해 주세요")
            return False
        elif self.textEditUuid.text() is "":
            self.msg_box("Uuid를 입력해 주세요")
            return False
        elif not isNumber(self.leMinPrice.text()) or not isNumber(self.leMaxPrice.text()):
            self.msg_box("가격을 입력해 주세요")
            return False
        elif not isNumber(self.leSelfVoume1.text()) or not isNumber(self.leSelfVoume2.text()):
            self.msg_box("수량을 입력해 주세요")
            return False
        elif not isNumber(self.textEditMinTime.text()) or not isNumber(self.textEditMaxTime.text()):
            self.msg_box("시간을 입력해 주세요")
            return False
        elif not isNumber(self.label_krw.text()) or not isNumber(self.label_have.text()):
            self.msg_box("계좌를 정상적으로 조회해 주세요")
            return False
        elif (float(self.leMinPrice.text()) * float(self.leSelfVoume1.text())) < 1000:
            self.msg_box("최소 금액 1000원 이상")
            return False
        elif (float(self.leMaxPrice.text()) * float(self.leSelfVoume2.text())) > float(self.label_krw.text()):
            self.msg_box("보유 원화보다 많은 자전금액")
            return False
        elif float(self.leSelfVoume2.text()) > float(self.label_have.text()):
            self.msg_box("보유 수량보다 많은 자전량")
            return False
        elif self.cbPointSelf.isChecked():
            if not self.leSelfVoume1.text().isdigit() or not self.leSelfVoume2.text().isdigit():
                self.msg_box("주문하는 수량이 소수점이면 소수점을 제거할 수 없습니다.")
                return False
        data = self.chainApi.best(self.coinIndex)
        if data is not None:
            bestAsk = data['best_ask_price']
            if bestAsk <= float(self.leMaxPrice.text()):
                self.msg_box("현재 매도 호가 범위를 침범합니다.")
                return False
            bestBid = data['best_bid_price']
            if bestBid >= float(self.leMinPrice.text()):
                self.msg_box("현재 매수 호가 범위를 침범합니다.")
                return False
        return True

    @staticmethod
    def msg_box(text):
        box = QMessageBox()
        box.setText(text)
        box.setWindowTitle("알림")
        box.exec_()


app = QApplication(sys.argv)
ChainApi = API("", "")
window = MyWindow()
window.setWindowTitle('ChainX Ver 1.02.24')
window.show()
app.exec_()


