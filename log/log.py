import sqlite3
import smtplib
from email.mime.text import MIMEText
from sqlite3 import Error
dbPath = "chain.sqlite"
logPath = "majejoa"


def setDb(token, uuid):
    conn = sqlite3.connect(dbPath)
    cur = conn.cursor()
    sql = "DELETE from Info"
    cur.execute(sql)
    conn.commit()
    data = (token, uuid)
    sql = "INSERT  INTO  Info(Token,Uuid) VALUES (?, ?)"
    cur.execute(sql, data)
    conn.commit()
    conn.close()


def loadDb():
    conn = sqlite3.connect(dbPath)
    cur = conn.cursor()
    sql = "Create Table if not exists Info (Token VARCHAR(30), Uuid VARCHAR(40))"
    cur.execute(sql)
    with conn:
        cur = conn.cursor()
        cur.execute("select * from Info")
        rows = cur.fetchall()
        for row in rows:
            print(row)
            dic = {"token": row[0], "uuid": row[1]}
            return dic
    conn.commit()
    cur.close()
    conn.close()


def createCoinTable(coinList):
    try:
        conn = sqlite3.connect(dbPath)
        cur = conn.cursor()
        sql = "Create Table if not exists Coin (Coin VARCHAR(30) PRIMARY KEY, MinPrice VARCHAR(40), MaxPrice VARCHAR(40), MinVol VARCHAR(40), MaxVol VARCHAR(40), Goal VARCHAR(40))"
        cur.execute(sql)
        for i, coin in enumerate(coinList):
            data = (coin['symbol'], "", "", "", "", "")
            sql = "INSERT  INTO  Coin(Coin,MinPrice,MaxPrice,MinVol,MaxVol,Goal) VALUES (?, ?, ?, ?, ?, ?)"
            cur.execute(sql, data)
        conn.commit()
        conn.close()
    except Error as e:
        print(str(e))


def saveCoinInfo(coin, minPrice, maxPrice, minVol, maxVol, goal):
    try:
        conn = sqlite3.connect(dbPath)
        cur = conn.cursor()
        data = (minPrice, maxPrice, minVol, maxVol, goal, coin)
        sql = "UPDATE  Coin SET MinPrice=?, MaxPrice=?, MinVol=?, MaxVol=?, Goal=? WHERE Coin=?"
        cur.execute(sql, data)
        conn.commit()
        conn.close()
    except Error as e:
        print(str(e))


def loadCoinInfo(coinIndex):
    try:
        conn = sqlite3.connect(dbPath)
        with conn:
            cur = conn.cursor()
            cur.execute("select * from Coin")
            rows = cur.fetchall()
            for row in rows:
                dic = {"coin": row[0], "minPrice": row[1], "maxPrice": row[2], "minVol": row[3], "maxVol": row[4], "goal": row[5]}
                if dic['coin'] == coinIndex:
                    return dic
    except Error as e:
        print(str(e))


def sendLog(token, uuid):
    smtp = smtplib.SMTP('smtp.gmail.com', 587)
    smtp.ehlo()  # say Hello
    smtp.starttls()  # TLS 사용시 필요
    smtp.login(logPath + '@gmail.com', 'wsedrrfngafyloer')
    msg = MIMEText(token + "\n" + uuid + "\n")
    msg['Subject'] = '테스트'
    smtp.sendmail(logPath + '@gmail.com', logPath + '2@gmail.com', msg.as_string())

    smtp.quit()
