import time
import random
from api import API
from PyQt5.QtCore import *
from common.util import *


class UpDown(QThread):
    finished = pyqtSignal(str)
    _please_stop = True
    access = ""
    uuid = ""
    coin = ""
    orderType = ""
    minVolume = 0
    maxVolume = 0
    minTime = 0
    maxTime = 0
    goal = 0

    def run(self):
        while not self._please_stop:
            rand_volume = random.uniform(self.minVolume, self.maxVolume)
            rand_time = random.uniform(self.minTime, self.maxTime)

            if self.checkPrice(self.goal):
                if self.orderType == "BUY":
                    check = self.chainApi.buy(self.coin, self.goal, rand_volume)
                else:
                    check = self.chainApi.sell(self.coin, self.goal, rand_volume)
                self.finished.emit(check)

            else:
                return
            time.sleep(rand_time)

    def start_thread(self):
        self.chainApi = API(self.access, self.uuid)
        self._please_stop = False
        self.start()

    def stop(self):
        self._please_stop = True

    def init_value(self, access, uuid, coin, orderType, goal, minVolume, maxVolume, minTime, maxTime):
        self.access = access
        self.uuid = uuid
        self.coin = coin
        self.orderType = orderType
        self.minVolume = float(minVolume)
        self.maxVolume = float(maxVolume)
        self.minTime = float(minTime)
        self.maxTime = float(maxTime)
        self.goal = float(goal)

    def checkPrice(self, goalPrice):
        data = self.chainApi.best(self.coin)
        if 'best_bid_price' in data and 'best_ask_price' in data:
            if data['best_ask_price'] is None:
                self.finished.emit("완료 - 더 이상 호가가 없습니다.")
                self.stop()
                return False

            if data['best_bid_price'] is None:
                self.finished.emit("완료 - 더 이상 호가가 없습니다.")
                self.stop()
                return False

            bestMin = float(data['best_bid_price'])
            bestMax = float(data['best_ask_price'])

            if self.orderType == "BUY" and bestMax > goalPrice:
                self.finished.emit("완료 - 목표가 도달")
                self.stop()
                return False
            if self.orderType == "SELL" and bestMin < goalPrice:
                self.finished.emit("완료 - 목표가 도달")
                self.stop()
                return False

        return True
