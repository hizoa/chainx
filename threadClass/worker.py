import time
import random
from api import API
from PyQt5.QtCore import *
from common.util import *


class Worker(QThread):
    finished = pyqtSignal(str)
    checked = pyqtSignal(float, float)
    _please_stop = True
    textEditAccess = ""
    textEditUuid = ""
    textEditCoin = ""
    minPrice = ""
    maxPrice = ""
    textEditStartVolume = ""
    textEditEndVolume = ""
    textMinTime = ""
    textMaxTime = ""
    checkedBid = False
    checkedAsk = False
    checkedPoint = False

    def run(self):
        while not self._please_stop:
            rand_volume = random.uniform(float(self.textEditStartVolume), float(self.textEditEndVolume))
            rand_price = get_gap_price(random.uniform(float(self.minPrice), float(self.maxPrice)))
            rand_time = random.uniform(float(self.textMinTime), float(self.textMaxTime))

            if self.checkedPoint:
                rand_volume = int(rand_volume)

            if self.checkPrice(rand_price):
                rand_order = random.randint(1, 3)
                if rand_order == 1:
                    check2 = self.chainApi.buy(self.textEditCoin, rand_price, rand_volume)
                    check1 = self.chainApi.sell(self.textEditCoin, rand_price, rand_volume)
                else:
                    check1 = self.chainApi.sell(self.textEditCoin, rand_price, rand_volume)
                    check2 = self.chainApi.buy(self.textEditCoin, rand_price, rand_volume)
                self.finished.emit(check1)
                self.finished.emit(check2)
                self.checked.emit(rand_volume, float(rand_price))
            else:
                return
            time.sleep(rand_time)

    def start_thread(self):
        self.chainApi = API(self.textEditAccess, self.textEditUuid)
        self._please_stop = False
        self.start()

    def stop(self):
        self._please_stop = True

    def init_value(self, access, uuid, coin, min_price, max_price, start_volume, end_volume, min_time, max_time, bid, ask, point):
        self.textEditAccess = access
        self.textEditUuid = uuid
        self.textEditCoin = coin
        self.minPrice = min_price
        self.maxPrice = max_price
        self.textEditStartVolume = start_volume
        self.textEditEndVolume = end_volume
        self.textMinTime = min_time
        self.textMaxTime = max_time
        self.checkedBid = bid
        self.checkedAsk = ask
        self.checkedPoint = point

    def checkPrice(self, randPrice):
        data = self.chainApi.best(self.textEditCoin)
        if 'best_bid_price' in data and 'best_ask_price' in data:
            bestMin = data['best_bid_price']
            bestMax = data['best_ask_price']
            price = float(randPrice)
            if bestMin is not None and float(bestMin) >= price:
                if self.checkedBid:
                    self.finished.emit("error_code:" + str(bestMin) + "에 매수세 감지")
                    print("여기를 두번?")
                    return False
                else:
                    self.finished.emit("감지:매수 (원화증가)추정")
            elif bestMax is not None and float(bestMax) <= price:
                if self.checkedAsk:
                    self.finished.emit("error_code:" + str(bestMax) + "에 매도세 감지")
                    return False
                else:
                    self.finished.emit("감지:매도 (원화감소)추정")
        return True
