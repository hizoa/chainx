from PyQt5.QtCore import *
from api import API
import random
from common import util
import time
from common.util import *


class OrderFlow(QThread):
    refresh = pyqtSignal()
    _please_stop = True
    minTime = 5
    maxTime = 5
    price = 10
    volume = 0
    token = ""
    uuid = ""
    coin = ""

    def run(self):
        chainApi = API(self.token, self.uuid)
        while not self._please_stop:
            last = chainApi.last(self.coin)
            if last is not None:
                lastPrice = get_gap_price(last['price'])
                gap = getGap(lastPrice)
                for num in range(1, 2):
                    chainApi.buy(self.coin, str(lastPrice - (gap * num)), 1)
                    chainApi.sell(self.coin, str(lastPrice + (gap * num)), 1)

            randTime = random.uniform(self.minTime, self.maxTime)
            self.refresh.emit()
            time.sleep(randTime)

    def startThread(self):
        self._please_stop = False
        self.start()

    def stop(self):
        self._please_stop = True

    def setTime(self, minTime, maxTime):
        if util.isNumber(minTime):
            self.minTime = minTime
        if util.isNumber(maxTime):
            self.maxTime = maxTime

    def setCoin(self, name):
        self.coin = name

    def isStop(self):
        return self._please_stop

    def setToken(self, token):
        self.token = token

    def setUuid(self, uuid):
        self.uuid = uuid

